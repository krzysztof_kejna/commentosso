﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MovoCreations.KrzysztofKejna.CommentoSSO.Models;
using Newtonsoft.Json;

namespace MovoCreations.KrzysztofKejna.CommentoSSO.Controllers
{
    public class SSOController : Controller
    {
        public IActionResult Commento()
        {
            if (User.Identity.Name != null)
            {
                var secretKey = StringToByteArray("4072926fbddbe6bd3406dab002692e5a15f587419e489c1ed44a216b968ec61f");
                string token = Request.Query["token"];
                var hmac = StringToByteArray(Request.Query["hmac"]);

                using(HMACSHA256 security = new HMACSHA256(secretKey))
                {
                    var expectedHmac = security.ComputeHash(StringToByteArray(token));
                    if (StructuralComparisons.StructuralEqualityComparer.Equals(hmac, expectedHmac))
                    {
                        var userAuth = new UserAuth();
                        userAuth.Email = User.Identity.Name;
                        int indexOfAt = userAuth.Email.IndexOf('@');
                        userAuth.Name = User.Identity.Name.Remove(indexOfAt);
                        userAuth.Token = token;

                        var json = JsonConvert.SerializeObject(userAuth);
                        var jsonHmac = ByteArrayToString(security.ComputeHash(Encoding.Default.GetBytes(json)));
                        var payload = ByteArrayToString(Encoding.Default.GetBytes(json));
                        var url = String.Join("", "https://commento.io/api/oauth/sso/callback?payload=", payload, "&hmac=", jsonHmac);
                        return Redirect(url);
                    }

                    return Redirect("error");
                }
            }

            return Redirect("error");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                .ToArray();
        }

        public string ByteArrayToString(byte[] byteArray)
        {
            StringBuilder hex = new StringBuilder(byteArray.Length * 2);
            foreach(byte b in byteArray)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }
    }
}
