﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovoCreations.KrzysztofKejna.CommentoSSO.Models
{
    public class UserAuth
    {
        public string Token;
        public string Email;
        public string Name;
        public string Link;
        public string Photo;
    }
}
